﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Управление волнами врагов
/// </summary>
public class WaveScript : MonoBehaviour
{


    /// <summary>
    /// Префаб "Враг"
    /// </summary>
    public Transform enemy;

    /// <summary>
    /// Время до появления первой волны волнами
    /// </summary>
    public float timeBeforeSpawning = 1.5f;


    /// <summary>
    /// Время между появлениями аргов в волне
    /// </summary>
    public float timeBetweenEnemies = 0.25f;

    /// <summary>
    /// Время между проверкой волн
    /// </summary>
    public float timeBeforeWaves = 2.0f;

    /// <summary>
    /// Количество врагов в волне
    /// </summary>
    public int enemiesPerWave = 10;

    /// <summary>
    /// Текущее количество врагов
    /// </summary>
    private int currentNumberOfEnemies = 0;


    // Use this for initialization
    void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Появление волн врагов
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawnEnemies()
    {
        // Начальная задержка перед первым появлением врагов
        yield return new WaitForSeconds(timeBeforeSpawning);
        // Когда таймер истекёт, начинаем производить эти действия
        while (true)
        {
            // Не создавать новых врагов, пока не уничтожены старые
            if (currentNumberOfEnemies <= 0)
            {
                float randDirection;
                float randDistance;
                // Создать N врагов в случайных местах за экраном
                for (int i = 0; i < enemiesPerWave; i++)
                {
                    // Задаём случайные переменные для расстояния и направления
                    randDistance = Random.Range(-10, 10);
                    randDirection = Random.Range(-10, 10);
                    // Используем переменные для задания координат появления врага
                    float posX = this.transform.position.x + randDistance;
                    float posY = this.transform.position.y + randDirection;
                    // Создаём врага на заданных координатах
                    Instantiate(enemy, new Vector3(posX, posY, 0), this.transform.rotation);
                    currentNumberOfEnemies++;
                    yield return new WaitForSeconds(timeBetweenEnemies);
                }
            }
            // Ожидание до следующей проверки
            yield return new WaitForSeconds(timeBeforeWaves);
            
        }
    }

    // Процедура уменьшения количества врагов 
    public void KillEnemy()
    {
        currentNumberOfEnemies--;
    }
}
