﻿using UnityEngine;


/// <summary>
/// Поведение префаба врага
/// </summary>
public class EnemyScript : MonoBehaviour
{


    /// <summary>
    /// Координаты игрока
    /// </summary>
    private Transform player;

    /// <summary>
    /// Контроллер управления волнами
    /// </summary>
    private WaveScript waveController;

    /// <summary>
    /// Скорость врага
    /// </summary>
    public float speed = 1.5f;


    /// <summary>
    /// Количество "жизни врага"
    /// </summary>
    public int health = 2;



    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("spaceship").transform;
        waveController = GameObject.FindGameObjectWithTag("GameController").GetComponent("WaveScript") as WaveScript;

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 delta = player.position - transform.position;
        delta.Normalize();
        float moveSpeed = speed * Time.deltaTime;
        transform.position = transform.position + (delta * moveSpeed);
    }


    void OnCollisionEnter2D(Collision2D theCollision)
    {
        //Проверяем коллизию с объектом типа «лазер»
        if (theCollision.gameObject.name.Contains("laser"))
        {
            LaserScript laser = theCollision.gameObject.GetComponent("LaserScript") as LaserScript;
            health -= laser.damage;
            Destroy(theCollision.gameObject);
        }
        if (health <= 0)
        {
            if (waveController != null)
                waveController.KillEnemy();

            Destroy(this.gameObject);
        }
    }
}
