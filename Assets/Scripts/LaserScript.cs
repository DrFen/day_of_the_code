﻿using UnityEngine;

/// <summary>
/// Поведение префаба лазера
/// </summary>
public class LaserScript : MonoBehaviour
{

    /// <summary>
    /// Время существования лазера
    /// </summary>
    public float lifetime = 2.0f;

    /// <summary>
    /// Скорость лазера
    /// </summary>
    public float speed = 5.0f;

    /// <summary>
    /// Урон лазера
    /// </summary>
    public int damage = 1;

    // Use this for initialization
    void Start()
    {
        Destroy(gameObject, lifetime);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector3.up * Time.deltaTime * speed);
    }
}
