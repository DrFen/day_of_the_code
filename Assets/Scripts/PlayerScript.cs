﻿using UnityEngine;

/// <summary>
/// Поведение  корабля игрока
/// </summary>
public class PlayerScript : MonoBehaviour
{

    #region camera
    /// <summary>
    /// Ограничение камеры по оси X минимум
    /// </summary>
    private float minX;

    /// <summary>
    /// Ограничение камеры по оси X максимум
    /// </summary>
    private float maxX;

    /// <summary>
    /// Ограничение камеры по оси Y минимум
    /// </summary>
    private float minY;

    /// <summary>
    /// Ограничение камеры по оси Y максимум 
    /// </summary>
    private float maxY;

    /// <summary>
    /// Половина размера корабля игрока
    /// </summary>
    private const float SIZE = 0.7f;
    #endregion camera

    #region movement
    /// <summary>
    /// Скорость корабля игрока
    /// </summary>
    public float playerSpeed = 2.0f;

    /// <summary>
    /// Кнопка вверх
    /// </summary>
    public KeyCode upButton;

    /// <summary>
    /// Кнопка вниз
    /// </summary>
    public KeyCode downButton;

    /// <summary>
    /// Кнопка влево
    /// </summary>
    public KeyCode leftButton;

    /// <summary>
    /// Кнопка вправо
    /// </summary>
    public KeyCode rightButton;

    #endregion movement


    #region shooting

    /// <summary>
    /// Кнопка выстрел
    /// </summary>
    public KeyCode shootButton;

    /// <summary>
    /// Префаб лазер
    /// </summary>
    public Transform laser;

    /// <summary>
    /// Задержка между выстрелами
    /// </summary>
    public float timeBetweenFires = 0.3f;    

    /// <summary>
    /// Как далеко от центра корабля будет появлятся лазер
    /// </summary>
    private const float laserDistance = 0.2f;


    /// <summary>
    /// Счетчик задержки между выстрелами
    /// </summary>
    private float timeTilNextFire = 0.0f;

    #endregion shooting


    // Use this for initialization
    void Start()
    {
        var worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0)); 
        minX = worldPoint.x *-1 + SIZE;
        maxX = worldPoint.x - SIZE;

        minY = worldPoint.y * -1 + SIZE;
        maxY = worldPoint.y - SIZE;

    }

    // Update is called once per frame
    void Update()
    {
        // Перемещение 
        Movement();

        //Выстрел
        Shoot();
    }

    // Движение героя 
    void Movement()
    {

        // Необходимое движение
        Vector3 movement = new Vector3();
        // Проверка нажатых клавиш
        movement += MoveIfPressed(upButton, Vector3.up);
        movement += MoveIfPressed(downButton, Vector3.down);
        movement += MoveIfPressed(leftButton, Vector3.left);
        movement += MoveIfPressed(rightButton, Vector3.right);
        // Если нажато несколько кнопок, обрабатываем это
        movement.Normalize();
        // Проверка нажатия кнопки
        if (movement.magnitude > 0)
        {
            // После нажатия двигаемся в этом направлении
            this.transform.Translate(movement * Time.deltaTime * playerSpeed, Space.World);
        }

         transform.position = new Vector3
         (
             Mathf.Clamp(transform.position.x, minX, maxX),
             Mathf.Clamp(transform.position.y, minY, maxY),
             transform.position.z
         );

    }

    /// <summary>
    /// Возвращает  вектор movement если нажата кнопка, или возврачает вектор (0;0;0)
    /// </summary>
    /// <param name="element">Кнопка для проверки</param>
    /// <param name="Movement">Возвращаемый вектор при условии</param>
    /// <returns></returns>
    Vector3 MoveIfPressed(KeyCode element, Vector3 movement)
    {
        
        if (Input.GetKey(element))
        {
            // Если нажато, покидаем функцию
            return movement;
        }
        // Если кнопки не нажаты, то не двигаемся
        return Vector3.zero;
    }

    /// <summary>
    /// Выстрел
    /// </summary>
    void Shoot()
    {
        if (Input.GetKey(shootButton) && timeTilNextFire <= 0)
        {
            timeTilNextFire = timeBetweenFires;
            Instantiate(laser, new Vector3(this.transform.position.x, this.transform.position.y, 0), this.transform.rotation);

        }

        timeTilNextFire -= Time.deltaTime;
    }



}
